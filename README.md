# @ridesz/usual-documentation-generator

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/ridesz/usual-documentation-generator/master)
![Libraries.io dependency status for latest release](https://img.shields.io/librariesio/release/npm/@ridesz/usual-documentation-generator)
![NPM](https://img.shields.io/npm/l/@ridesz/usual-documentation-generator)
![npm type definitions](https://img.shields.io/npm/types/@ridesz/usual-documentation-generator)
![npm](https://img.shields.io/npm/dw/@ridesz/usual-documentation-generator)

The typical documentation generator config I use for my TypeScript projects.

## How to use

### `package.json` setup

Just please add this `scripts` tag part to your `package.json` file too:

```json
{
    "doc": "npx @ridesz/usual-documentation-generator@latest",
}
```

### GitLab CI config

Please add the necessary parts to your `.gitlab-ci.yml` file:

```yml
image: node:lts

stages:
    - documentation

documentation:
    stage: documentation
    script:
        - npm install
        - npm run doc
    artifacts:
        paths:
            - reports/docs
        expire_in: 1 week
```

This would use the latest node image and do the documentation generation as part of the `documentation` stage.

### Running manually


If you would like to run the documentation generation manually then you can use the following command:

```bash
npm run doc
```

## Some implementation details

The documentation is generated into the `reports/docs` folder.
