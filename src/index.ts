#!/usr/bin/env node

import * as shellJs from "shelljs";
import resolve from "enhanced-resolve";
import { dirname } from "path";
import debug from "debug";

const debugLogger = debug("@ridesz/usual-documentation-generator");

function getProjectFolder(): string {
    const result = shellJs.pwd();
    if (result.code !== 0) {
        throw new Error("Can't find the current folder where I am");
    }
    const currentFolderWhereIAm = result.stdout.replace(/\s/g, "");
    debugLogger(`Current folder where I am: ${currentFolderWhereIAm}`);
    // The `currentFolderWhereIAm` is a folder, we don't need `dirname(...)` for it...
    const projectRoot = findUpProjectRoot(currentFolderWhereIAm);
    debugLogger(`Project root for the current folder: ${projectRoot}`);
    return dirname(projectRoot);
}

function removeOldFolder(): void {
    const result = shellJs.exec("npx rimraf ./reports/docs");
    if (result.code !== 0) {
        throw new Error("Can't delete the old docs");
    }
}

function findUpProjectRoot(folder: string): string {
    const result = shellJs.exec(`npx find-up-cli package.json --cwd=${folder}`, { silent: true });
    if (result.code !== 0) {
        throw new Error("Can't do find up");
    }
    return result.stdout.replace(/\s/g, "");
}

function getImportedProjectFolderByName(name: string): string {
    const resolved = resolve.sync(__dirname, name);
    if (resolved === false) {
        throw new Error("Can't get the NPX project folder path");
    }
    return dirname(findUpProjectRoot(dirname(resolved)));
}

function runDocsGeneration(): void {
    debugLogger(`Getting project folder...`);
    const projectFolder = getProjectFolder();
    debugLogger(`Project folder: ${projectFolder}`);
    debugLogger(`Getting typedoc folder...`);
    const typedocFolder = getImportedProjectFolderByName("typedoc");
    debugLogger(`Typedoc folder: ${typedocFolder}`);
    const entryPoint = `${projectFolder}/src`;
    const outputFolder = `${projectFolder}/reports/docs`;
    const tsconfigPath = `${projectFolder}/tsconfig.json`;
    const result = shellJs
        .cd(typedocFolder)
        .exec(`./bin/typedoc ${entryPoint} --out ${outputFolder} --tsconfig ${tsconfigPath} --includeVersion`);
    if (result.code !== 0) {
        throw new Error("Can't generate docs");
    }
}

try {
    console.log();
    console.log("Generating documentation...");

    removeOldFolder();

    runDocsGeneration();

    console.log();
    console.log("Documentation generation finished.");
    console.log();
    shellJs.exit(0);
} catch (error) {
    console.log();
    console.log(`ERROR WITH THE DOCUMENTATION GENERATION: ${(error as Error).message}`);
    console.log();
    shellJs.exit(1);
}
