# Change Log

All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

### Changed

### Fixed

- Fixing README.md (#4)

## [1.0.3] - 2021-11-02

### Added

### Changed

### Fixed

- Better debug logging (#4)

## [1.0.2] - 2021-11-02

### Added

### Changed

### Fixed

- Node lts in the README.md too (#7)
- Still fixing the broken localhost (#4)

## [1.0.1] - 2021-11-02

### Added

### Changed

### Fixed

- Fixing broken localhost (#4)

## [1.0.0] - 2021-11-02

### Added

- Adding some project labels to the readme.md (#8)
- Npm version scripts (#9)

### Changed

- BREAKING CHANGE: Node v16 required (#6)
- GitLab CI based on node lts (#7)

### Fixed

- Fixing broken localhost (#4)
- Missing exports (#5)

## [0.0.5] - 2021-10-30

### Added

### Changed

### Fixed

- Updating some dependencies (#3)

## [0.0.4] - 2021-02-05

### Added

### Changed

### Fixed

- Supporting GitLab CI

## [0.0.3] - 2021-02-01

### Added

### Changed

### Fixed

- Fixing project description

## [0.0.2] - 2021-02-01

### Added

### Changed

- Initializing the project

### Fixed
